=========================
spacenet_image_downloader
=========================


.. image:: https://img.shields.io/pypi/v/spacenet_image_downloader.svg
        :target: https://pypi.python.org/pypi/spacenet_image_downloader

.. image:: https://img.shields.io/travis/00krishna/spacenet_image_downloader.svg
        :target: https://travis-ci.org/00krishna/spacenet_image_downloader

.. image:: https://readthedocs.org/projects/spacenet-image-downloader/badge/?version=latest
        :target: https://spacenet-image-downloader.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




download spacenet images from aws.


* Free software: MIT license
* Documentation: https://spacenet-image-downloader.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
