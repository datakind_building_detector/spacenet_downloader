# -*- coding: utf-8 -*-

"""Console script for spacenet_image_downloader."""
import click
from click.testing import CliRunner
from .BuildingsDownloader import BuildingsDownloader
from .ConfigFileManager import ConfigFileManager
import logging
import traceback
import os
from os import path, remove
import json
import subprocess
import pytest
import click_log


CONTEXT_SETTINGS = {'DIRECTORY': os.getcwd(),
                    'LOGGER': logging.getLogger(__name__),
                    'CONFIG_FILE': os.path.join(os.getcwd(),'spacenet_config.json')}

clicklogger = logging.getLogger(__name__)
click_log.basic_config(clicklogger)

@click.group()
@click.option('-d',
              '--directory',
              help='Target directory for download',
              type=click.Path(exists=True),
              prompt='Enter the Target directory for the file download',
              default=CONTEXT_SETTINGS['DIRECTORY'])
@click.option('-c',
              '--configfile',
              help='Path and filename for spacenet configuration file',
              type=click.Path(),
              prompt='Enter the Path and Name of an existing spacenet config file if it exists',
              default=CONTEXT_SETTINGS['CONFIG_FILE'])
@click_log.simple_verbosity_option(clicklogger)
def main(directory, configfile):
    """This package will download Spacenet Building Imagery that is currently
    hosted on the Amazon Cloud in its free S3 storage bucket.

    The data downloaded include a directory of high resolution satellite
    images as well as building footprint labels in geojson format. This
    data may be used for machine learning and similar applications.

    Note that the configuration file is a JSON file that contains
    information on the locations of the downloaded spacenet files
    for subsequent imagery pre-processing. If no file is specified,
    then a new file is created in the download directory.
    """


    if directory is not None:
        CONTEXT_SETTINGS['DIRECTORY'] = directory
    if configfile is None:
        CONTEXT_SETTINGS['CONFIG_FILE'] = os.path.join(directory, 'spacenet_config.json')
    elif os.path.isdir(configfile):
        CONTEXT_SETTINGS['CONFIG_FILE'] = os.path.join(configfile, 'spacenet_config.json')
    elif os.path.isfile(configfile):
        CONTEXT_SETTINGS['CONFIG_FILE'] = configfile

    clicklogger.info('Target directory is: ' + CONTEXT_SETTINGS['DIRECTORY'])
    clicklogger.info('Config File location is : ' + CONTEXT_SETTINGS['CONFIG_FILE'])
#    if path.isfile("python_logging.log"):
#        remove("python_logging.log")


@click.command('lasvegas', short_help='Download Las Vegas imagery and labels')
def lasvegas():
    """
    This subcommand will download the Las Vegas imagery zip file to the
    specified directory.
    """
    CONTEXT_SETTINGS['country'] = 'lasvegas'
    bd = BuildingsDownloader(CONTEXT_SETTINGS['DIRECTORY'])
    bd.download_lasvegas()
    update_config_file()

@click.command('paris', short_help='Download Paris imagery and labels')
def paris():

    CONTEXT_SETTINGS['country'] = 'paris'
    bd = BuildingsDownloader(CONTEXT_SETTINGS['DIRECTORY'])
    bd.download_paris()
    update_config_file()

@click.command('shanghai', short_help='Download Shanghai imagery and labels')
def shanghai():

    CONTEXT_SETTINGS['country'] = 'shanghai'
    bd = BuildingsDownloader(CONTEXT_SETTINGS['DIRECTORY'])
    bd.download_shanghai()
    update_config_file()

@click.command('khartoum', short_help='Download Khartoum imagery and labels')
def khartoum():

    CONTEXT_SETTINGS['country'] = 'khartoum'
    bd = BuildingsDownloader(CONTEXT_SETTINGS['DIRECTORY'])
    bd.download_khartoum()
    update_config_file()

def update_config_file():
    config = ConfigFileManager(CONTEXT_SETTINGS)
    config.write_config_file()



main.add_command(lasvegas)
main.add_command(paris)
main.add_command(shanghai)
main.add_command(khartoum)


class Tests(object):
    def test_las_vegas_download(self):
        runner = CliRunner()
        result = runner.invoke(main, ['lasvegas'])
        assert 'Preparing to download Las Vegas imagery' in result.output

if __name__ == "__main__":
    main()  # pragma: no cover
