# -*- coding: utf-8 -*-

"""Top-level package for spacenet_image_downloader."""

__author__ = """krishna bhogaonker"""
__email__ = 'cyclotomiq@gmail.com'
__version__ = '0.1.0'

from os import path, remove
import logging.config
import json
from .BuildingsDownloader import BuildingsDownloader
from .ConfigFileManager import ConfigFileManager

# If applicable, delete the existing log file to generate a fresh log file during each execution
if path.isfile("python_logging.log"):
    remove("python_logging.log")

with open("spacenet_downloader_logging_config.json", 'r') as logging_configuration_file:
    config_dict = json.load(logging_configuration_file)

logging.config.dictConfig(config_dict)

# Log that the logger was configured
logger = logging.getLogger(__name__)
logger.info('Completed configuring logger()!')
