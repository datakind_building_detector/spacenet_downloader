#!/usr/bin/python

"""

Spacenet imagery downloader class.

"""

## MIT License
##
## Copyright (c) 2018, krishna bhogaonker
## Permission is hereby granted, free of charge, to any person obtaining a ## copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

## The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


__author__ = 'krishna bhogaonker'
__copyright__ = 'copyright 2018'
__credits__ = ['krishna bhogaonker']
__license__ = "MIT"
__version__ = '0.1.0'
__maintainer__ = 'krishna bhogaonker'
__email__ = 'cyclotomiq@gmail.com'
__status__ = 'pre-alpha'

import logging
import traceback
import os
import boto3
import wget
import pytest




class BuildingsDownloader:

    def __init__(self, directory):
        self.logger = logging.getLogger(__name__)
        self.directory = directory
        os.chdir(directory)

    def download_lasvegas(self):
        """
        Downloads spacenet las vegas building images and label zipfile
        from aws.

        Returns:
        zipfile with images/labels in the present working directory.
        """
        self.logger.info('Start request to download Las Vegas buildings.')
        try:
            bucket = 'spacenet-dataset'
            key = 'AOI_2_Vegas/AOI_2_Vegas_Train.tar.gz'
            s3 = boto3.client('s3')
            url = s3.generate_presigned_url(ClientMethod='get_object',
                                            Params={'Bucket': bucket, 'Key': key, 'RequestPayer': 'requester'},
                                            ExpiresIn=3600 * 360)
            wget.download(url)
        except Exception as e:
            print('Error in downloading imagery')
            print("type error: " + str(e))
            print(traceback.format_exc())
            self.logger.warning('ERROR: '+ str(e) + 'Download did not complete.')
        else:
            self.logger.info('Finished downloading Las Vegas buildings.')

    def download_paris(self):
        """
        Downloads spacenet paris building images and label zipfile
        from aws.
        Returns:
        zipfile with images/labels in the present working directory.

        """
        self.logger.info('Downloading Paris buildings.')
        try:
            bucket = 'spacenet-dataset'
            key = 'AOI_3_Paris/AOI_3_Paris_Train.tar.gz'
            s3 = boto3.client('s3')
            url = s3.generate_presigned_url(ClientMethod='get_object',
                                            Params={'Bucket': bucket, 'Key': key, 'RequestPayer': 'requester'},
                                            ExpiresIn=3600 * 360)
            wget.download(url)

        except Exception as e:
            print('Error in downloading imagery')
            print("type error: " + str(e))
            print(traceback.format_exc())
            self.logger.warning('ERROR: '+ str(e) + 'Download did not complete.')
        else:
            self.logger.info('Finished downloading Paris buildings.')

    def download_shanghai(self):
        """
        Downloads spacenet Shanghai building images and label zipfile
        from aws.

        Returns:
        zipfile with images/labels in the present working directory.
        """
        self.logger.info('Downloading Shanghai buildings.')
        try:
            bucket = 'spacenet-dataset'
            key = 'AOI_4_Shanghai/AOI_4_Shanghai_Train.tar.gz'
            s3 = boto3.client('s3')
            url = s3.generate_presigned_url(ClientMethod='get_object',
                                            Params={'Bucket': bucket, 'Key': key, 'RequestPayer': 'requester'},
                                            ExpiresIn=3600 * 360)
            wget.download(url)
        except Exception as e:
            print('Error in downloading imagery')
            print("type error: " + str(e))
            print(traceback.format_exc())
            self.logger.warning('ERROR: '+ str(e) + 'Download did not complete.')
        else:
            self.logger.info('Finished downloading Shanghai buildings.')

    def download_khartoum(self):

        """
        Downloads spacenet Khartoum building images and label zipfile
        from aws.

        Returns:
        zipfile with images/labels in the present working directory.
        """
        self.logger.info('Downloading Khartoum buildings.')
        try:
            bucket = 'spacenet-dataset'
            key = 'AOI_5_Khartoum/AOI_5_Khartoum_Train.tar.gz'
            s3 = boto3.client('s3')
            url = s3.generate_presigned_url(ClientMethod='get_object',
                                            Params={'Bucket': bucket, 'Key': key, 'RequestPayer': 'requester'},
                                            ExpiresIn=3600 * 360)
            wget.download(url)
        except Exception as e:
            print('Error in downloading imagery')
            print("type error: " + str(e))
            print(traceback.format_exc())
            self.logger.warning('ERROR: '+ str(e) + 'Download did not complete.')
        else:
            self.logger.info('Finished downloading Khartoum buildings.')


class Tests(object):
    downloader = BuildingsDownloader(os.getcwd())
    dir = os.getcwd()

    def test_las_vegas(self):
        self.downloader.download_lasvegas()


if __name__ == "__main__":
    pass
