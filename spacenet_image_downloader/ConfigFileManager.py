#!/usr/bin/python

"""

This module will manage the config file for the Spacenet Downloader and preprocessor.

"""

## MIT License
##
## Copyright (c) 2017, krishna bhogaonker
## Permission is hereby granted, free of charge, to any person obtaining a ## copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

## The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


__author__ = 'krishna bhogaonker'
__copyright__ = 'copyright 2018'
__credits__ = ['krishna bhogaonker']
__license__ = "MIT"
__version__ = '0.1.0'
__maintainer__ = 'krishna bhogaonker'
__email__ = 'cyclotomiq@gmail.com'
__status__ = 'pre-alpha'

import logging
import json
import os
import traceback


CONFIG_FILE_TEMPLATE = {'lasvegas': {},
                        'paris': {},
                        'shanghai': {},
                        'khartoum': {}}

IMAGERYFILENAMES = {'lasvegas': 'AOI_2_Vegas_Train.tar.gz',
                    'paris': 'AOI_3_Paris_Train.tar.gz',
                    'shanghai': 'AOI_4_Shanghai_Train.tar.gz',
                    'khartoum': 'AOI_5_Khartoum_Train.tar.gz'}


class ConfigFileManager:
    def __init__(self, CONTEXT_SETTINGS):
        self.country = CONTEXT_SETTINGS.pop('country', None)
        self.logger = logging.getLogger(__name__)
        self.config_file_path = CONTEXT_SETTINGS['CONFIG_FILE']
        self.download_file_path = os.path.join(CONTEXT_SETTINGS['DIRECTORY'],
                                               IMAGERYFILENAMES[self.country])
        self.config = {}
        if os.path.isfile(self.config_file_path):
            self.read_config_file()
        else:
            self.config = CONFIG_FILE_TEMPLATE
        self.add_zipfile_to_config()

    def read_config_file(self):
        try:
            with open('data.json') as infile:
                self.config = json.load(infile)
        except Exception as e:
            raise InvalidConfigFile(e)

    def write_config_file(self):

        try:
            with open(self.config_file_path, 'w') as outfile:
                json.dump(self.config, outfile)
        except Exception as e:
            print('Error in writing config file.')
            print(traceback.format_exc())

    def add_zipfile_to_config(self):
        self.config[self.country]['path_zipfile'] = self.download_file_path


class Error(Exception):
   """Base class for exceptions in this module."""
   pass

class InvalidConfigFile(Error):
   def __init__(self, evalue):
       print('The package could not read the config file. Please confirm that the files is properly written in JSON or use the Spacenet_Image_Downloader software and let the program write the config file for you.')
       print('Error Message: ' + str(evalue))
       print(traceback.format_exc())


class Tests(object):

   def test_1(self):
       pass




if __name__ == "__main__":
   pass

